<?php 
  require_once("include/header.php");
  require_once("database/service.php");

  // pre($dataHistory);
?>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Histori
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Hasil</li>
      </ol>
    </section>
    <section class="content">
<div class="panel panel-default">
  <div class="table-responsive">
    <table class="table table-bordered table-hover table-striped">
    <thead>
      <tr>
        <th>No.</th>
        <th>Riwayat Kepemimpinan</th>
        <th>Nama Alternatif Terpilih</th>
        <th>Aksi</th>
      </tr>
      <?php foreach ($dataHistory as $key => $value):?>
        <tr>
          <td><?= $value['id_history'];?></td>
          <td><?= 'Riwayat Kepemimpinan '.$value['id_history'];?></td>
          <td><?= $value['nama'];?></td>
          <td><a class="btn btn-xs btn-warning" href="detailHistory.php?history=<?= $value['id_history']?>"><span class="glyphicon glyphicon-search"></span> Detail History</a></td>
        </tr>
        <?php endforeach?>
    </thead>  
    </table>
  </div>
</div>
    </section>

<?php require_once("include/footer.php"); ?>
  