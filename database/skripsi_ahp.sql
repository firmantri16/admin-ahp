-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 19 Mar 2019 pada 17.25
-- Versi server: 10.1.34-MariaDB
-- Versi PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi_ahp`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_alternatif`
--

CREATE TABLE `tbl_alternatif` (
  `kode_alternatif` varchar(10) NOT NULL,
  `nip_alternatif` varchar(100) NOT NULL,
  `nama_alternatif` varchar(100) NOT NULL,
  `email_alternatif` varchar(100) NOT NULL,
  `telepon_alternatif` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_alternatif`
--

INSERT INTO `tbl_alternatif` (`kode_alternatif`, `nip_alternatif`, `nama_alternatif`, `email_alternatif`, `telepon_alternatif`) VALUES
('A1', '35', 'Mulyono', 'mulyono@gmail.com', '098423432'),
('A2', '40', 'Paijo', 'paijo@gmail.com', '089324234'),
('A3', '23', 'Asep', 'asep@gmail.com', '0897898989');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_alternatif_proses`
--

CREATE TABLE `tbl_alternatif_proses` (
  `id` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nip` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telepon` varchar(100) NOT NULL,
  `nilai` float NOT NULL,
  `status` varchar(100) NOT NULL,
  `id_history` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_alternatif_proses`
--

INSERT INTO `tbl_alternatif_proses` (`id`, `kode`, `nip`, `nama`, `email`, `telepon`, `nilai`, `status`, `id_history`) VALUES
(31, 'A1', '35', 'Mulyono', 'mulyono@gmail.com', '098423432', 0.34, 'Terpilih', 1),
(32, 'A2', '40', 'Paijo', 'paijo@gmail.com', '089324234', 0.34, 'Tidak Terpilih', 1),
(33, 'A3', '23', 'Asep', 'asep@gmail.com', '0897898989', 0.34, 'Tidak Terpilih', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_history`
--

CREATE TABLE `tbl_history` (
  `id_history` int(11) NOT NULL,
  `tgl_proses` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_history`
--

INSERT INTO `tbl_history` (`id_history`, `tgl_proses`) VALUES
(1, '2019-03-18 22:07:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kriteria`
--

CREATE TABLE `tbl_kriteria` (
  `kode_kriteria` varchar(10) NOT NULL,
  `nama_kriteria` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_kriteria`
--

INSERT INTO `tbl_kriteria` (`kode_kriteria`, `nama_kriteria`) VALUES
('K1', 'Kinerja'),
('K2', 'Prestasi'),
('K3', 'Loyalitas'),
('K4', 'Disiplin'),
('K5', 'asdasd'),
('K6', 'Rudi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_relasi`
--

CREATE TABLE `tbl_relasi` (
  `id` int(11) NOT NULL,
  `kode_alternatif` varchar(10) DEFAULT NULL,
  `kode_kriteria` varchar(10) DEFAULT NULL,
  `nilai_bobot` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `password`) VALUES
(1, 'admin', 'admin'),
(2, 'firman', 'firman'),
(3, 'agus', 'agus');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_alternatif`
--
ALTER TABLE `tbl_alternatif`
  ADD PRIMARY KEY (`kode_alternatif`);

--
-- Indeks untuk tabel `tbl_alternatif_proses`
--
ALTER TABLE `tbl_alternatif_proses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_history` (`id_history`);

--
-- Indeks untuk tabel `tbl_history`
--
ALTER TABLE `tbl_history`
  ADD PRIMARY KEY (`id_history`);

--
-- Indeks untuk tabel `tbl_kriteria`
--
ALTER TABLE `tbl_kriteria`
  ADD PRIMARY KEY (`kode_kriteria`);

--
-- Indeks untuk tabel `tbl_relasi`
--
ALTER TABLE `tbl_relasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode_alternatif` (`kode_alternatif`),
  ADD KEY `kode_kriteria` (`kode_kriteria`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_alternatif_proses`
--
ALTER TABLE `tbl_alternatif_proses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT untuk tabel `tbl_history`
--
ALTER TABLE `tbl_history`
  MODIFY `id_history` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_relasi`
--
ALTER TABLE `tbl_relasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tbl_alternatif_proses`
--
ALTER TABLE `tbl_alternatif_proses`
  ADD CONSTRAINT `tbl_alternatif_proses_ibfk_1` FOREIGN KEY (`id_history`) REFERENCES `tbl_history` (`id_history`);

--
-- Ketidakleluasaan untuk tabel `tbl_relasi`
--
ALTER TABLE `tbl_relasi`
  ADD CONSTRAINT `tbl_relasi_ibfk_1` FOREIGN KEY (`kode_alternatif`) REFERENCES `tbl_alternatif` (`kode_alternatif`),
  ADD CONSTRAINT `tbl_relasi_ibfk_2` FOREIGN KEY (`kode_kriteria`) REFERENCES `tbl_kriteria` (`kode_kriteria`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
