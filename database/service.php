<?php require_once 'database/koneksi.php';

/**
 *Select Data */
//ambil data dari tabel kriteria
$kriteria = query ("SELECT * FROM tbl_kriteria");
$alternatif = query ("SELECT * FROM tbl_alternatif");
$jmlKriteria = query("SELECT COUNT(*) as jmlKriteria FROM tbl_kriteria");
$jmlAlternatif = query("SELECT COUNT(*) as jmlAlternatif FROM tbl_alternatif");
$jmlRecordHistory = query("SELECT COUNT(*) as record FROM tbl_history");
$dataHistory = tampilHistoryTerpilih();

function query($query)
{
	global $koneksi;
	$result = mysqli_query($koneksi, $query);
	$rows = [];
	while ($row = mysqli_fetch_assoc($result)) {
		$rows[] = $row;
	}
	return $rows;
}

/*KRITERIA*/

function kriteriaTambah($data) {
	//ambil data dari tiap elemen dalam form
	global $koneksi;

	$kkriteria = $data["kode_kriteria"];
	$nkriteria = $data["nama_kriteria"];

	//query insert data
	$queryKriteria = "INSERT INTO tbl_kriteria VALUES
		('$kkriteria', '$nkriteria')";
		
	mysqli_query($koneksi, $queryKriteria);
	return mysqli_affected_rows($koneksi);
}
	
function kriteriaHapus($kodekriteria){
	global $koneksi;
	mysqli_query ($koneksi, "DELETE FROM tbl_kriteria WHERE kode_kriteria = '$kodekriteria'");
	return mysqli_affected_rows($koneksi);
}

function kriteriaUbah($data){
	global $koneksi;
	$kode_kriteria_lama = $data["kode_kriteria_lama"];
	$kkriteria = $data["kode_kriteria"];
	$nkriteria = $data["nama_kriteria"];
	
	//query ubah data
	$queryKriteria = "UPDATE tbl_kriteria SET
									kode_kriteria = '$kkriteria',
									nama_kriteria = '$nkriteria'
									WHERE kode_kriteria = '$kode_kriteria_lama';
								";

	mysqli_query($koneksi, $queryKriteria);

	return mysqli_affected_rows($koneksi);
}

function alternatifTambah($data) {
	//ambil data dari tiap elemen dalam form
	global $koneksi;

	$kalternatif = $data["kode_alternatif"];
	$nip = $data["nip_alternatif"];
	$nalternatif = $data["nama_alternatif"];
	$email = $data["email_alternatif"];
	$tlp = $data["telepon_alternatif"];

	//query insert data
	$queryAlternatif = "INSERT INTO tbl_alternatif 
						VALUES
					('$kalternatif','$nip', '$nalternatif', '$email', '$tlp')";
	mysqli_query($koneksi, $queryAlternatif);

	for($i = 1; $i<=3; $i++){
	$queryRelasi = "INSERT INTO tbl_relasi 
					VALUES ('','K$i', '$kalternatif', '0')";
	mysqli_query($koneksi, $queryRelasi);
	}

	return mysqli_affected_rows($koneksi);
		
}

function alternatifHapus($kodealternatif){
	global $koneksi;
	mysqli_query ($koneksi, "DELETE FROM tbl_alternatif WHERE kode_alternatif = '$kodealternatif'");
	return mysqli_affected_rows($koneksi);
}


function alternatifUbah($data){
	global $koneksi;
	$kode_alternatif_lama = $data["kode_alternatif_lama"];
	$kalternatif = $data["kode_alternatif"];
	$nip = $data["nip_alternatif"];
	$nalternatif = $data["nama_alternatif"];
	$email = $data["email_alternatif"];
	$tlp = $data["telepon_alternatif"];

	//query ubah data
	$queryAlternatif = "UPDATE tbl_alternatif SET
									kode_alternatif = '$kalternatif',
									nip_alternatif = '$nip',
									nama_alternatif = '$nalternatif',
									email_alternatif = '$email',
									telepon_alternatif = '$tlp'
									WHERE kode_alternatif = '$kode_alternatif_lama';
								";

	mysqli_query($koneksi, $queryAlternatif);

	return mysqli_affected_rows($koneksi);
}

function historyTambah($arr, $rec){
	global $koneksi;
	$id = $rec[0]['record']+1;
	$datetime = date("Y-m-d H:i:s");
	// $nama = 'agus';
	$queryHistory = "INSERT INTO tbl_history (id_history, tgl_proses) VALUES ($id, '$datetime')";
	mysqli_query($koneksi, $queryHistory);
// echo $id;
	foreach ($arr as $key => $value) {
		$kode = $value['kode_alternatif'];
		$nip = $value['nip_alternatif'];
		$nama = $value['nama_alternatif'];
		$email = $value['email_alternatif'];
		$telepon = $value['telepon_alternatif'];
		$nilai = $value['0'];
		$status = $value['1'];
		$queryAlternatifProses = "INSERT INTO tbl_alternatif_proses (kode, nip, nama, email, telepon, nilai,status, id_history) VALUES ('$kode', '$nip', '$nama', '$email', '$telepon', '$nilai','$status', $id)";
		mysqli_query($koneksi, $queryAlternatifProses);
	}
}

function tampilHistoryTerpilih(){
	global $koneksi;
	$matrix=[];
	$query = "SELECT h.id_history, alt.nama FROM tbl_history h JOIN tbl_alternatif_proses alt ON h.id_history = alt.id_history WHERE alt.status = 'Terpilih'";
	$result = mysqli_query($koneksi, $query);
	
	if (mysqli_num_rows($result) > 0) {
		// output data of each row
		while($row = mysqli_fetch_assoc($result)) {
			array_push($matrix, $row);
		}
	}

	return $matrix;
}

function tampilDetailHistory($id){
	global $koneksi;
	$matrix = [];
	$query = "SELECT kode, nip, nama, email, telepon, nilai, status FROM `tbl_alternatif_proses` WHERE id_history = $id";
	$result = mysqli_query($koneksi, $query);
	
	if (mysqli_num_rows($result) > 0) {
		// output data of each row
		while($row = mysqli_fetch_assoc($result)) {
			array_push($matrix, $row);
		}
	}

	return $matrix;
}
?>