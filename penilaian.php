<?php 
require_once("include/header.php");
require_once("database/service.php");

$jmlK = $jmlKriteria[0]['jmlKriteria'];
$jmlA = $jmlAlternatif[0]['jmlAlternatif'];
function showOption(){
  echo "
    <option value=".'0'.">0</option>
    <option value=".'1'." selected=".'selected'.">1</option>
    <option value=".'0.5'.">1/2</option>
    <option value=".'0.33'.">1/3</option>
    <option value=".'0.25'.">1/4</option>
    <option value=".'0.2'.">1/5</option>
    <option value=".'0.16'.">1/6</option>
    <option value=".'0.14'.">1/7</option>
    <option value=".'0.125'.">1/8</option>
    <option value=".'0.11'.">1/9</option>
    <option value=".'2'.">2</option>
    <option value=".'3'.">3</option>
    <option value=".'4'.">4</option>
    <option value=".'5'.">5</option>
    <option value=".'6'.">6</option>
    <option value=".'7'.">7</option>
    <option value=".'8'.">8</option>
    <option value=".'9'.">9</option>
  ";
}
?>
<!-- Content Wrapper. Contains page content -->
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Penilaian
      <small>Tables Pair Comparation</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Penilaian</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-warning">
          <div class="panel-heading">
            <div class="form-group">
              <h4>Tabel Skala Perbandingan</h4>
            </div>
          </div>
          <div class="table-responsive">
            <table class="table table-hover table-bordered table-striped">
              <thead>
                <tr>
                  <th>Intensitas Kepentingan</th>
                  <th>Definisi</th>
                  <th>Penjelasan</th>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Kedua elemen sama pentingnya</td>
                  <td>Dua elemen memiliki pengaruh yang sama besar</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Elemen yang satu sedikit lebih penting daripada elemen lainnya</td>
                  <td>Pengalaman dan pertimbangan sedikit menyokong satu elemen atas elemen lainnya</td>
                </tr>
                <tr>
                  <td>5</td>
                  <td>Elemen yang satu lebih penting daripada elemen lainnya</td>
                  <td>Pengalaman dan pertimbangan dengan kuat menyokong satu elemen atas elemen yang lainnya</td>
                </tr>
                <tr>
                  <td>7</td>
                  <td>Satu Elemen jelas lebih penting ketimbang elemen lainnya</td>
                  <td>Satu elemen dengan kuat disokong, dan dominannya telah terlihat dalam praktek</td>
                </tr>
                <tr>
                  <td>9</td>
                  <td>Satu elemen mutlak lebih penting ketimbang elemen yang lainnya</td>
                  <td>Bukti yang menyokong elemen yang satu atas yang lainnya memiliki tingkat penegasan tertinggi yang mungkin menguatkan</td>
                </tr>
                <tr>
                  <td>2,4,6,8</td>
                  <td>Nilai-nilai diantara dua pertimbangan yang berdekatan</td>
                  <td>Nilai ini diberikan bila ada dua kompromi diantara dua pilihan</td>
                </tr>
              </thead>
          </table>
        </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Pairwise Kriteria - Kriteria -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-info">
          <div class="panel-heading">
            <div class="form-group">
              <h4>Tabel Penilaian Matriks Kriteria</h4>
              <h5>Matriks Kriteria</h5>
            </div>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <form method="post" action="olahdata.php">
                <table class="table table-bordered table-striped table-hover" border="3">
                  <tr>
                    <td></td>
                    <?php 
                      foreach ($kriteria as $index => $value) {
                          echo "<td>".$value['nama_kriteria']."</td>";
                      }
                    ?>
                  </tr>
                  <?php foreach ($kriteria as $index => $value): ?>
                  <tr>
                    <td>
                      <?= $value['nama_kriteria']?>
                    </td>
                    <?php for ($i=0; $i <$jmlK; $i++): ?>
                    <td>
                      <div class="form-group">
                        <select name="kriteria[<?= $value['nama_kriteria']?>][<?= $i?>]" class="form-control">
                          <?php showOption();?>
                        </select>
                      </div>
                    </td>
                    <?php endfor?>
                  </tr>
                  <?php endforeach?>
                </table>
                <!-- </form> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!--Pairwise Kriteria dan Alternatif-->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-info">
          <div class="panel-heading">
            <div class="form-group">
              <h4>Tabel Penilaian Matriks Alternatif</h4>
              <h5>Matriks Alternatif terhadap Kriteria</h5>
            </div>
          </div>
          <div class="pane-body">
            <div class="table-responsive">
              <!-- <form method="post" action="test.php"> -->
              <?php for ($x=0; $x <$jmlK; $x++): ?>
              <div class="panel-heading">
                <b>
                  <?php echo $kriteria[$x]['nama_kriteria']?></b>
              </div>
              <table class="table table-bordered table-striped table-hover" border="3 ">
                <tr>
                  <td></td>
                  <?php 
                  // echo "<pre>";
                  // print_r($alternatif);
                  // echo "</pre>";
                  foreach ($alternatif as $index => $value) {
                      echo "<td>".$value['nama_alternatif']."</td>";
                  }?>
                </tr>
                <?php foreach ($alternatif as $index => $value): ?>
                <tr>
                  <td>
                    <?= $value['nama_alternatif']?>
                  </td>
                  <?php for ($a=0; $a <$jmlA; $a++): ?>
                  <td>
                    <div class="form-group">
                      <select name="mix[<?= $kriteria[$x]['nama_kriteria']?>][<?= $value['nama_alternatif']?>][<?= $a ?>]"
                        class="form-control">
                        <?php showOption();?>
                      </select>
                    </div>
                  </td>
                  <?php endfor?>
                </tr>
                <?php endforeach?>
              </table>
              <?php endfor?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="text-center"><button type="submit" class="btn btn-success">Proses Data</button></div>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</section>
</div>


<?php require_once 'include/footer.php'; ?>