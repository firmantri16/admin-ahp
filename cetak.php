<html>
<head>
	<title>Cetak Laporan</title>
</head>
<body>
 
	<center>
 
		<h2>DATA LAPORAN KRITERIA</h2>
 
	</center>
 
	<?php 
	require_once 'database/koneksi.php';
	?>
 
	<table border="1" style="width: 100%">
		<tr>
			<th>Kode kriteria</th>
			<th>Nama kriteria</th>
		</tr>
		<?php 
		$sql = mysqli_query($koneksi,"select * from tbl_kriteria");
		while($data = mysqli_fetch_array($sql)){
		?>
		<tr>
			<td><?php echo $data['kode_kriteria']; ?></td>
			<td><?php echo $data['nama_kriteria']; ?></td>
		</tr>
		<?php 
		}
		?>
	</table>
 
	<script>
		window.print();
	</script>
 
</body>
</html>